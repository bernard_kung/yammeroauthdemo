package com.yammer.samples.oauth;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Build;
import android.util.Log;
import android.os.StrictMode; 

public class yammeruser extends Activity implements OnClickListener {
String accessToken;
static EditText et;
	
@Override
public void onCreate(Bundle savedInstanceState) {

	
super.onCreate(savedInstanceState);

Intent intent = getIntent();
accessToken = intent.getStringExtra(OAuthLoginActivity.ACCESS_TOKEN);

StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
StrictMode.setThreadPolicy(policy);

setContentView(R.layout.output);
et = (EditText)findViewById(R.id.my_edit);

findViewById(R.id.my_button).setOnClickListener(this);
}


@Override
public void onClick(View arg0) {
//Button b = (Button)findViewById(R.id.my_button);
//b.setClickable(false);
	//JSONObject test1 = requestWebService("https://www.yammer.com/api/v1/users/current.json?access_token="+accessToken);
	JSONObject test1 = requestWebService("https://www.yammer.com/api/v1/messages/in_group/996037.json?threaded=true&access_token="+accessToken);	
}


public static JSONObject requestWebService(String serviceUrl) {
    //disableConnectionReuseIfNecessary();
	String allresults=null;
 
    HttpURLConnection urlConnection = null;
    try {
        // create connection
        URL urlToRequest = new URL(serviceUrl);
        urlConnection = (HttpURLConnection) 
            urlToRequest.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setReadTimeout(5000);
         
        // handle issues
        int statusCode = urlConnection.getResponseCode();
        if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
            // handle unauthorized (if service requires user login)
        } else if (statusCode != HttpURLConnection.HTTP_OK) {
            // handle any other errors, like 404, 500,..
        }
         
        // create JSON object from content
        //InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        
        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        
        String line = null;
        StringBuilder sb = new StringBuilder();
        
        while((line = in.readLine()) != null) {
          System.out.println(line);
          sb.append(line + "\n");
        }
        allresults = sb.toString();
        
        /*
        JSONObject jObject = new JSONObject(allresults);
        String firstname = jObject.getString("first_name");
        String lastname=jObject.getString("last_name");
        String location=jObject.getString("location");
        
        addResults("First Name:" + firstname + "\n");
        addResults("Last Name:" + lastname + "\n");
        addResults("Location:" + location + "\n");
        */
        
        JSONObject jObject = new JSONObject(allresults);
        JSONArray jsonArray = jObject.getJSONArray("messages");
        
        for (int i=0; i < jsonArray.length(); i++)
        {
            try {
                JSONObject oneObject = jsonArray.getJSONObject(i);
                // Pulling items from the array
                String bodys = oneObject.getJSONObject("body").getString("plain").replace("\n", " ");
                String tbody;
                if(bodys.length()>=50)
                tbody=bodys.substring(0,50)+"...";
                else
                tbody=bodys;

                addResults("Message "+ (i + 1) + " : " + tbody + "\n\n\n");
                //String oneObjectsItem2 = oneObject.getString("anotherSTRINGNAMEINtheARRAY");
            } catch (JSONException e) {
                // Oops
            }
        }


        //return new JSONObject(getResponseText(in));
        return null; 
    } catch (MalformedURLException e) {
    	System.out.println("11111111111111111111111");
        // URL is invalid
    } catch (SocketTimeoutException e) {
    	System.out.println("222222222222222222222222");
        // data retrieval or connection timed out
    } catch (IOException e) {
    	System.out.println("333333333333333333333333333");
        // could not read response body 
        // (could not create input stream)
    } catch (JSONException e) {
        // response body is no valid JSON string
    } finally {
        if (urlConnection != null) {
            urlConnection.disconnect();
        }
    }       
     
    return null;
}


private static String getResponseText(InputStream inStream) {
    // very nice trick from
    // http://weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html
	
    return new Scanner(inStream).useDelimiter("\\A").next();
}


private static void addResults(String results) {
	if (results!=null) {
	et.append(results);
	}
}


}