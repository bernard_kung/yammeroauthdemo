package com.yammer.samples.oauth;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.yammer.samples.contactlist.view.ContactListActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;
import android.util.Log;
import android.os.StrictMode; 
import android.provider.ContactsContract;

public class userdetails extends Activity{
String accessToken;
String id;
String email;
private static final String MAP_URL = "file:///android_asset/userdetails.html";
private WebView webView2;

String name="";
String address="";
String workphone="";
String mobilephone="";
String workemail="";
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	
super.onCreate(savedInstanceState);

Intent intent = getIntent();
//accessToken = intent.getStringExtra(groupuser.ACCESS_TOKEN);
//id= intent.getStringExtra(groupuser.USER_ID);
//accessToken = intent.getStringExtra(ContactListActivity.ACCESS_TOKEN);

MyApp appState = ((MyApp)getApplicationContext());
accessToken=appState.getAccessToken();

email=intent.getStringExtra(ContactListActivity.USER_EMAIL);

StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
StrictMode.setThreadPolicy(policy);


setContentView(R.layout.user_details);
//System.out.println("https://www.yammer.com/api/v1/users/"+id+".json?access_token="+accessToken);
//requestWebService("https://www.yammer.com/api/v1/users/"+id+".json?access_token="+accessToken);

//by_email.json?email=
requestWebService("https://www.yammer.com/api/v1/users/by_email.json?email="+ email+ "&access_token="+accessToken);
//System.out.println("https://www.yammer.com/api/v1/users/by_email.json?email="+ email+ "&access_token="+accessToken);
}


@SuppressLint("JavascriptInterface")
public void requestWebService(String serviceUrl) {
    //disableConnectionReuseIfNecessary();
	String allresults=null;
 
    HttpURLConnection urlConnection = null;
    try {
        // create connection
        URL urlToRequest = new URL(serviceUrl);
        urlConnection = (HttpURLConnection) 
            urlToRequest.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setReadTimeout(5000);
         
        // handle issues
        int statusCode = urlConnection.getResponseCode();
        if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
            // handle unauthorized (if service requires user login)
        } else if (statusCode != HttpURLConnection.HTTP_OK) {
            // handle any other errors, like 404, 500,..
        }
         
        // create JSON object from content
        //InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        
        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        
        String line = null;
        StringBuilder sb = new StringBuilder();
        
        while((line = in.readLine()) != null) {
          System.out.println(line);
          sb.append(line + "\n");
        }
        allresults = sb.toString();
        //JSONObject jObject = new JSONObject(allresults);
        
        JSONArray j0=new JSONArray(allresults);
        JSONObject jObject = j0.getJSONObject(0);
        
        name = jObject.getString("full_name");
        address=jObject.getString("location");
        JSONArray phone_numbers = jObject.getJSONObject("contact").getJSONArray("phone_numbers");
        JSONArray email_addresses = jObject.getJSONObject("contact").getJSONArray("email_addresses");
        
        
        
        for(int i = 0; i < phone_numbers.length(); i++)
        {
        	JSONObject phone = phone_numbers.getJSONObject(i);
        	if(phone.getString("type").equals("work"))
        		workphone=phone.getString("number");
        	else
        		mobilephone=phone.getString("number");
        	
        }
        
        for(int i = 0; i < email_addresses.length(); i++)
        {
        	JSONObject emails = email_addresses.getJSONObject(i);
        	if(emails.getString("type").equals("primary"))
        	{	
        		workemail=emails.getString("address");
        		break;
        	}
        }
        
        System.out.println("Work:"+workphone);
        System.out.println("Mobile"+mobilephone);
        System.out.println("Email"+workemail);
        
        webView2 = (WebView) findViewById(R.id.webview2);
        webView2.getSettings().setJavaScriptEnabled(true);//Webview JavaScript
        webView2.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("tel:")) { 
                        Intent intent = new Intent(Intent.ACTION_DIAL,
                                Uri.parse(url)); 
                        startActivity(intent); 
                }
                else if (url.startsWith("mailto:")){
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(userdetails.this, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                    startActivity(i);
                    //view.reload();
                }else if(url.startsWith("http:") || url.startsWith("https:")) {
                    view.loadUrl(url);
                }
                return true;
            }
            
            public Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
                intent.putExtra(Intent.EXTRA_TEXT, body);
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.putExtra(Intent.EXTRA_CC, cc);
                intent.setType("message/rfc822");
                return intent;
            }
            
        });        
        webView2.loadUrl("javascript:var thisname='"+name+"'; " +
        		"var thisaddress='"+address+"'; " +
        		"var thisworkphone='"+workphone+"'; " +
        		"var thismobilephone='"+mobilephone+"'; " +
        		"var thisworkemail='"+workemail+"';");
        
        webView2.loadUrl(MAP_URL);  //URL 
        
        webView2.addJavascriptInterface(new Object()
    	{
    	
        @SuppressWarnings("unused")
		public void performClick()
    	  {
    	    // Deal with a click on the OK button = Add Contact
    		  String DisplayName = name;
    		  String MobileNumber = mobilephone;
    		  String HomeNumber = "";
    		  String WorkNumber = workphone;
    		  String emailID = workemail;
    		  String company = "Telstra";
    		  String jobTitle = "";

    		  ArrayList < ContentProviderOperation > ops = new ArrayList < ContentProviderOperation > ();

    		  ops.add(ContentProviderOperation.newInsert(
    		  ContactsContract.RawContacts.CONTENT_URI)
    		      .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
    		      .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
    		      .build());

    		  //------------------------------------------------------ Names
    		  if (DisplayName != null) {
    		      ops.add(ContentProviderOperation.newInsert(
    		      ContactsContract.Data.CONTENT_URI)
    		          .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
    		          .withValue(ContactsContract.Data.MIMETYPE,
    		      ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
    		          .withValue(
    		      ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
    		      DisplayName).build());
    		  }

    		  //------------------------------------------------------ Mobile Number                     
    		  if (MobileNumber != null) {
    		      ops.add(ContentProviderOperation.
    		      newInsert(ContactsContract.Data.CONTENT_URI)
    		          .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
    		          .withValue(ContactsContract.Data.MIMETYPE,
    		      ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
    		          .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, MobileNumber)
    		          .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
    		      ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
    		          .build());
    		  }

    		  //------------------------------------------------------ Home Numbers
    		  if (HomeNumber != null) {
    		      ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
    		          .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
    		          .withValue(ContactsContract.Data.MIMETYPE,
    		      ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
    		          .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, HomeNumber)
    		          .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
    		      ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
    		          .build());
    		  }

    		  //------------------------------------------------------ Work Numbers
    		  if (WorkNumber != null) {
    		      ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
    		          .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
    		          .withValue(ContactsContract.Data.MIMETYPE,
    		      ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
    		          .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, WorkNumber)
    		          .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
    		      ContactsContract.CommonDataKinds.Phone.TYPE_WORK)
    		          .build());
    		  }

    		  //------------------------------------------------------ Email
    		  if (emailID != null) {
    		      ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
    		          .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
    		          .withValue(ContactsContract.Data.MIMETYPE,
    		      ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
    		          .withValue(ContactsContract.CommonDataKinds.Email.DATA, emailID)
    		          .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
    		          .build());
    		  }

    		  //------------------------------------------------------ Organization
    		  if (!company.equals("") && !jobTitle.equals("")) {
    		      ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
    		          .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
    		          .withValue(ContactsContract.Data.MIMETYPE,
    		      ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
    		          .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, company)
    		          .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
    		          .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, jobTitle)
    		          .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
    		          .build());
    		  }

    		  // Asking the Contact provider to create a new contact                 
    		  try {
    		      getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
    		      Toast.makeText(getApplicationContext(), "Added to Contact List Successfully!", Toast.LENGTH_SHORT).show();
    		  } catch (Exception e) {
    		      e.printStackTrace();
    		      Toast.makeText(getApplicationContext(), "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
    		  }   
    		  
    		  
    		  
    	  }
    	}, "addtocontact");
        
        
        

    } catch (MalformedURLException e) {
    	System.out.println("======================URL is invalid======================");
        // URL is invalid
    } catch (SocketTimeoutException e) {
    	System.out.println("======================Data retrieval or connection timed out======================");
        // data retrieval or connection timed out
    } catch (IOException e) {
    	System.out.println("======================Could not read response body======================");
        // could not read response body 
        // (could not create input stream)
    } catch (JSONException e) {
        // response body is no valid JSON string
    } finally {
        if (urlConnection != null) {
            urlConnection.disconnect();
        }
    }       
}




}