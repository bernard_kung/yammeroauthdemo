package com.yammer.samples.oauth;

import android.app.Application;

public class MyApp extends Application {

	  private String accesstoken;

	  public String getAccessToken(){
	    return accesstoken;
	  }
	  public void setAccessToken(String s){
		  accesstoken = s;
	  }
	  
}
