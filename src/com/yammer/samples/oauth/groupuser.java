package com.yammer.samples.oauth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class groupuser extends Activity{
public final static String USER_ID = "com.yammer.samples.oauth.UID";
public final static String ACCESS_TOKEN = "com.yammer.samples.oauth.ACCESSTOKEN";
final Context context = this;
String accessToken;
static EditText et;
ListView listView ;
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	
super.onCreate(savedInstanceState);

Intent intent = getIntent();
accessToken = intent.getStringExtra(OAuthLoginActivity.ACCESS_TOKEN);

StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
StrictMode.setThreadPolicy(policy);


setContentView(R.layout.groupuser_layout);
//Get ListView object from xml
listView = (ListView) findViewById(R.id.list);

requestWebService("https://www.yammer.com/api/v1/users/in_group/996037.json?access_token="+accessToken);
}


public void requestWebService(String serviceUrl) {
    //disableConnectionReuseIfNecessary();
	String allresults=null;
 
    HttpURLConnection urlConnection = null;
    try {
        // create connection
        URL urlToRequest = new URL(serviceUrl);
        urlConnection = (HttpURLConnection) 
            urlToRequest.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setReadTimeout(5000);
         
        // handle issues
        int statusCode = urlConnection.getResponseCode();
        if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
            // handle unauthorized (if service requires user login)
        } else if (statusCode != HttpURLConnection.HTTP_OK) {
            // handle any other errors, like 404, 500,..
        }
         
        // create JSON object from content
        //InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        
        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        
        String line = null;
        StringBuilder sb = new StringBuilder();
        
        while((line = in.readLine()) != null) {
          System.out.println(line);
          sb.append(line + "\n");
        }
        allresults = sb.toString();
                
        JSONObject jObject = new JSONObject(allresults);
        JSONArray jsonArray = jObject.getJSONArray("users");
        
        String[] values=new String[jsonArray.length()];
        final String[] uid=new String[jsonArray.length()];
        
        for (int i=0; i < jsonArray.length(); i++)
        {
            try {
                JSONObject oneObject = jsonArray.getJSONObject(i);
                // Pulling items from the array
                String names = oneObject.getString("full_name");
                values[i]=names;
                uid[i]=oneObject.getString("id");
                
            } catch (JSONException e) {
                // Oops
            }
        }
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, values);
        
     // Assign adapter to ListView
        listView.setAdapter(adapter); 
        
        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

              @Override
              public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
              /*  
               // ListView Clicked item index
               int itemPosition     = position;
               
               // ListView Clicked item value
               String  itemValue    = (String) listView.getItemAtPosition(position);
                  
                // Show Alert 
                Toast.makeText(getApplicationContext(),
                  "Position :"+itemPosition+"  ListItem : " +uid[position] , Toast.LENGTH_LONG)
                  .show();*/
            	  
            	  Intent intent = new Intent(context, userdetails.class); 
                  intent.putExtra(USER_ID, uid[position]);
                  intent.putExtra(ACCESS_TOKEN, accessToken);
                  startActivity(intent);	  
            	  
             
              }
        });

    } catch (MalformedURLException e) {
    	System.out.println("======================URL is invalid======================");
        // URL is invalid
    } catch (SocketTimeoutException e) {
    	System.out.println("======================Data retrieval or connection timed out======================");
        // data retrieval or connection timed out
    } catch (IOException e) {
    	System.out.println("======================Could not read response body======================");
        // could not read response body 
        // (could not create input stream)
    } catch (JSONException e) {
        // response body is no valid JSON string
    } finally {
        if (urlConnection != null) {
            urlConnection.disconnect();
        }
    }       
}


}