package com.yammer.samples.oauth;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.yammer.samples.contactlist.view.ContactListActivity;

import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.MotionEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class OAuthLoginActivity extends Activity {
	public final static String ACCESS_TOKEN = "com.yammer.samples.oauth.MESSAGE";
	private final static String YAMMER_URL="https://www.yammer.com/dialog/oauth?client_id=iibkZ8NXn8wzc0OerjZ2g&response_type=token";
	String accessToken;
	private WebView mWebView;
	
	
	
	final Context context = this;
	// UI references.
    //private WebView mWebView;
    
	
	 @Override
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if(event.getAction() == KeyEvent.ACTION_DOWN){
	            switch(keyCode)
	            {
	            case KeyEvent.KEYCODE_BACK:
	                if(mWebView.canGoBack() == true){
	                    mWebView.goBack();
	                }else{
	                    finish();
	                }
	                return true;
	            }

	        }
	        return super.onKeyDown(keyCode, event);
	    }
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminateVisibility(true); 
		
		setContentView(R.layout.activity_oauth_login);
	
		/* Testing if Yammer OAuth website is available */
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		try {
	        URL surl = new URL(YAMMER_URL);
	        executeReq(surl);
	        loadYammerLogin();
	    }
	    catch(Exception e) {
	        Toast.makeText(getApplicationContext(), "Network connection error", Toast.LENGTH_SHORT).show();
	        super.onBackPressed();
	    }


    }
	
	
	@Override
	protected void onResume() {
		super.onResume();
		loadYammerLogin();
    }
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.oauth_login, menu);
		return true;
	}
	
	private void executeReq(URL urlObject) throws IOException
	{
	    HttpURLConnection conn = null;
	    conn = (HttpURLConnection) urlObject.openConnection();
	    conn.setReadTimeout(30000);//milliseconds
	    conn.setConnectTimeout(3500);//milliseconds
	    conn.setRequestMethod("GET");
	    conn.setDoInput(true);

	    // Start connect
	    conn.connect();
	    InputStream response =conn.getInputStream();
	    Log.d("Response:", response.toString());
	}
	
	
	private void loadYammerLogin()
	{
		mWebView = (WebView)findViewById(R.id.mywebview);
		mWebView.getSettings().setJavaScriptEnabled(true);  
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.loadUrl(YAMMER_URL);
	}
	
	
	private void saveAccessToken(String url) {
        // extract the token if it exists
		
		try{
		
			String loginstatus="";
			
			if (url.contains("access_token=")) {
				String fragment = "access_token=";
	            int start = url.indexOf(fragment);
	            if (start > -1) {
	
	                // You can use the accessToken for api calls now.
	                accessToken = url.substring(start + fragment.length(), url.length());
	                loginstatus="Login Success, access token is " + accessToken;
	                
	                //Intent intent = new Intent(context, groupuser.class); 
	                
	                
	                //Testing on global
	                MyApp appState = (MyApp)getApplicationContext();
	                appState.setAccessToken(accessToken);
	                
	                
	                /* for contact list testing   */
	                Intent intent = new Intent(context, ContactListActivity.class);
	                intent.putExtra(ACCESS_TOKEN, accessToken);
	                startActivity(intent);
	                
	                
	                /* for yammer feed testing
	                setContentView(R.layout.activity_oauth_login);
	        		url="file:///android_asset/yammerfeed.html";        		
	        		 
	        		mWebView = (WebView)findViewById(R.id.mywebview);
	        		mWebView.getSettings().setJavaScriptEnabled(true);  
	        		mWebView.setOnTouchListener(new View.OnTouchListener() {
	        			@Override
	        		    public boolean onTouch(View v, MotionEvent event) {
	                      return (event.getAction() == MotionEvent.ACTION_MOVE);
	                    }
	                });
	        		
	        		mWebView.setWebViewClient(new NormalWebClient());
	        		mWebView.setWebChromeClient(new WebChromeClient());
	                
	                
	                
	                mWebView.loadUrl(url);*/
	                
	            }
	            
	            System.out.println(url);
	            
	        }
			else
			{
				loginstatus="Login Failure";
				System.out.println(loginstatus);
			}
		
		}
		catch(Exception e)
		{
			System.out.println("System Error: "+ e.toString());
		}
		
		//AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
		//alertbox.setMessage(loginstatus);
		//alertbox.create();
		//alertbox.show();
		
		
        
    }
	
	private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //check if the login was successful and the access token returned
            //this test depend of your API
            if (url.contains("access_token=")) {
                //save your token
                saveAccessToken(url);
                return true;
            }
            //BaseActivity.logEvent(Consts.EVENT_CALLBACK + "Login Failed", true);
            return false;
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {	
			 
        	Toast.makeText(getBaseContext(), "Network Error", Toast.LENGTH_SHORT).show();
        	//404 : error code for Page Not found
			  //if(errorCode==404){
			    // show Alert here for Page Not found
			    //view.loadUrl("file:///android_asset/Page_Not_found.html");
				  
			  //}
			  //System.out.println(errorCode);

			}
        
        
    }
	
	
	private class NormalWebClient extends WebViewClient {
		@Override
	    public boolean shouldOverrideUrlLoading(WebView webview, String url){
	            
	            if (url != null && url.startsWith("https://")) {
	                webview.getContext().startActivity(
	                    new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
	                return true;
	            } else {
	            	webview.loadUrl(url);
		            setProgressBarIndeterminateVisibility(true);
	                return false;
	            }
	        }
	    
		@Override
	    public void onPageFinished(WebView webview, String url){
	            super.onPageFinished(webview, url);
	            setProgressBarIndeterminateVisibility(false);
	    }
    }
	
	
	public String getAccessToken(){
	    return accessToken;
	  }
	

}
