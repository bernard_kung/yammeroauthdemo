package com.yammer.samples.contactlist.models;

import com.yammer.samples.contactlist.widget.ContactItemInterface;

public class ExampleContactItem implements ContactItemInterface{

	private String nickName;
	private String fullName;
	private String email;
	
	public ExampleContactItem(String nickName, String fullName, String email) {
		super();
		this.nickName = nickName;
		this.setFullName(fullName);
		this.email=email;
	}

	// index the list by nickname
	@Override
	public String getItemForIndex() {
		return nickName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getEmail() {
		return email;
	}
	

}
