package com.yammer.samples.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DBAdapter {

	// Database fields
	/*public static final String KEY_JB_ID = "jb_id";
	public static final String KEY_JB_ACRONYM = "jb_acronym";
	public static final String KEY_JB_TERM = "jb_term";
	public static final String KEY_JB_EXPLANATION = "jb_explanation";*/
	private Context context;
	private SQLiteDatabase database;
	private JBDBHelper dbHelper;

	public DBAdapter(Context context) {
		this.context = context;
	}

	public DBAdapter open() throws SQLException {
		if (dbHelper == null) {
			dbHelper = new JBDBHelper(context);
		}

		database = dbHelper.getReadableDatabase();

		return this;
	}

	public void close() {
		if (dbHelper != null)
			dbHelper.close();
	}

	/**
	 * Return a Cursor over the list of all terms and acronyms filtered by input
	 * in the database
	 * 
	 * @return Cursor over all terms acronyms
	 */

	public Cursor fetchDB() {

		return database.rawQuery(String.format(JBDBHelper.GRAD_DB_SQL), null);
	}
	

}
