package com.yammer.samples.database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class JBDBHelper extends SQLiteAssetHelper {

	private static final String DATABASE_NAME = "starterappdb";
	private static final int DATABASE_VERSION = 1;

	public static String GRAD_DB_SQL = "select * from grad_db";

	public JBDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
}
